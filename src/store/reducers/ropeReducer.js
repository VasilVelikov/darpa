const initState = {
    leftPlayerDistance: 100,
    rightPlayerDistance: 100,
};

const ropeReducer = (state = initState, action) => {
    switch (action.type) {
        case "REDUCE_LEFT":
            if (state.leftPlayerDistance > 0 && state.rightPlayerDistance > 0) {
                return {
                    ...state,
                    leftPlayerDistance: state.leftPlayerDistance - 2,
                }
            } else {
                return {
                    ...state
                }
            }
        case "REDUCE_RIGHT":
            if (state.rightPlayerDistance > 0 && state.leftPlayerDistance > 0) {
                return {
                    ...state,
                    rightPlayerDistance: state.rightPlayerDistance - 2,
                }
            } else {
                return {
                    ...state
                }
            }
        case "RESTART":
            return {
                ...state,
                leftPlayerDistance: action.distance,
                rightPlayerDistance: action.distance,
            }
        default:
            return state
    }
}

export default ropeReducer;