import ropeReducer from './ropeReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    rope: ropeReducer
})

export default rootReducer;