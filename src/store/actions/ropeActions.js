export const reduceLeftDistance = () => {
    return (dispatch, getState) => {
        dispatch({ type: "REDUCE_LEFT" })
    }
}

export const reduceRightDistance = () => {
    return (dispatch, getState) => {
        dispatch({ type: "REDUCE_RIGHT" })
    }
}

export const restartDistances = (distance) => {
    return (dispatch, getState) => {
        dispatch({ type: "RESTART", distance })
    }
}