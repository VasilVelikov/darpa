import React, { Component } from 'react';
import '../css/components/home.css';
import Canvas from './gameboard/Canvas';
import Buttons from './gameboard/Buttons';
import { connect } from 'react-redux';
import Distances from './gameboard/Distances';

class Home extends Component {

    render() {
        const distances = { left: this.props.leftPlayerDistance, right: this.props.rightPlayerDistance };
        return (
            <div id="home-container">
                <span id="title">DARPA</span>
                <Distances distances={distances}></Distances>
                <Canvas distances={distances}></Canvas>
                <Buttons distances={distances}></Buttons>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        leftPlayerDistance: state.rope.leftPlayerDistance,
        rightPlayerDistance: state.rope.rightPlayerDistance,
    }
}

export default connect(mapStateToProps)(Home);