import React, { Component } from 'react';
import '../../css/components/gameboard/canvas.css';
import { connect } from 'react-redux';
import { restartDistances } from '../../store/actions/ropeActions';
import EndgameMessage from './EndgameMessage';

class Canvas extends Component {

    componentDidMount() {
        let canvas = document.getElementById("board");
        this.setState({
            canvasWidth: canvas.width
        });
        this.props.restartDistances(20 / 100 * canvas.width);
        this.drawCanvas();
    }

    componentDidUpdate() {
        this.drawCanvas();
    }

    drawCanvas = () => {
        const distances = this.props.distances;
        let canvas = document.getElementById("board");
        if (canvas) {
            let context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);

            //0.5s fix the blurry lines
            const playerSquareSide = 60;
            const leftPlayerX = canvas.width / 2 - distances.left - playerSquareSide + 0.5;
            const rightPlayerX = canvas.width / 2 + distances.right + 0.5;
            const playersY = canvas.height - playerSquareSide - 50 + 0.5;

            //player squares
            context.beginPath();
            context.strokeStyle = "black";
            context.rect(leftPlayerX, playersY, playerSquareSide, playerSquareSide);
            context.fillStyle = `rgb(${204 - 204 * distances.left / 60}, ${204 * distances.left / 60}, 0)`;
            context.fill();
            context.stroke();

            context.beginPath();
            context.rect(rightPlayerX, playersY, playerSquareSide, playerSquareSide);
            context.fillStyle = `rgb(${204 - 204 * distances.right / 60}, ${204 * distances.right / 60}, 0)`;
            context.fill();
            context.stroke();

            //center vertical line
            context.beginPath();
            context.moveTo(canvas.width / 2 + 0.5, canvas.height - playerSquareSide - 60);
            context.lineTo(canvas.width / 2 + 0.5, canvas.height + 0.5 - 50);

            //bottom line
            context.moveTo(0.5, canvas.height + 0.5 - 50);
            context.lineTo(canvas.width, canvas.height + 0.5 - 50);
            context.stroke();

            //red connecting line
            context.beginPath();
            context.strokeStyle = "orange";
            context.moveTo(leftPlayerX + playerSquareSide / 2 + 0.5, playersY + playerSquareSide / 2);
            context.lineTo(rightPlayerX + playerSquareSide / 2 + 0.5, playersY + playerSquareSide / 2);
            context.stroke();
        }
    }

    render() {
        const distances = this.props.distances;
        return (
            <>
                {distances.left <= 0 || distances.right <= 0 ?
                    distances.left <= 0 ?
                        <EndgameMessage winningPlayer={2} restartDistance={20 / 100 * this.state.canvasWidth}></EndgameMessage>
                        :
                        <EndgameMessage winningPlayer={1} restartDistance={20 / 100 * this.state.canvasWidth}></EndgameMessage>
                    :
                    <canvas id="board">
                        Your browser does not support the HTML5 canvas tag.
                    </canvas>
                }
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        restartDistances: distance => dispatch(restartDistances(distance)),
    }
}

export default connect(null, mapDispatchToProps)(Canvas);