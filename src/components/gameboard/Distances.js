import React, { Component } from 'react';
import '../../css/components/gameboard/distances.css';

class Distances extends Component {

    render() {
        const distances = this.props.distances;
        return (
            <div id="distances-container">
                <div className="left-player-container">
                    <p className="player">Player 1</p>
                    <span className="distance-title">Distance</span>
                    <span className="distance">{distances.left}</span>
                </div>
                <div className="right-player-container">
                    <p className="player">Player 2</p>
                    <span className="distance-title">Distance</span>
                    <span className="distance">{distances.right}</span>
                </div>
            </div>
        )
    }
}

export default Distances;