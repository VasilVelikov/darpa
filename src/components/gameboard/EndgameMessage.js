import React, { Component } from 'react';
import '../../css/components/gameboard/endgameMessage.css';
import { connect } from 'react-redux';
import { restartDistances } from '../../store/actions/ropeActions';

class EndgameMessage extends Component {

    render() {
        return (
            <>
                {<div id="endgame-container">
                    <span id="endgame-message">Player {this.props.winningPlayer} wins!</span>
                    <span id="new-game-button" onClick={() => this.props.restartDistances(this.props.restartDistance)}>New Game</span>
                </div>}
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        restartDistances: distance => dispatch(restartDistances(distance)),
    }
}

export default connect(null, mapDispatchToProps)(EndgameMessage);