import React, { Component } from 'react';
import '../../css/components/gameboard/buttons.css';
import { connect } from 'react-redux';
import { reduceLeftDistance, reduceRightDistance } from '../../store/actions/ropeActions';

class Buttons extends Component {

    componentDidMount() {
        const reduceRight = this.props.reduceRightDistance;
        const reduceLeft = this.props.reduceLeftDistance;

        document.removeEventListener('keyup', keyboardPress);
        document.addEventListener('keyup', keyboardPress);

        function keyboardPress(e) {
            switch (e.code) {
                case "KeyD":
                    reduceRight();
                    break;
                case "KeyP":
                    reduceLeft();
                    break;
                default:
                    console.log(e.code);
            }
        }
    }

    render() {
        const distances = this.props.distances;
        return (
            <>
                {distances.left > 0 && distances.right > 0 ?
                    <div id="buttons-container">
                        <div>
                            <span>press</span>
                            <span className="distance-button" onClick={this.props.reduceRightDistance}>D</span>
                        </div>
                        <div>
                            <span>press</span>
                            <span className="distance-button" onClick={this.props.reduceLeftDistance}>P</span>
                        </div>
                    </div>
                    : null}
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reduceLeftDistance: () => dispatch(reduceLeftDistance()),
        reduceRightDistance: () => dispatch(reduceRightDistance()),
    }
}

export default connect(null, mapDispatchToProps)(Buttons);